# Torre Pokémon

Este programa hace uso de librerías OpenGl para dibujar la "TorrePokémon", además de implementar distintas cámaras con distintos puntos de vista y permitir controlar el movimiento mediante teclado.

Este proyecto contiene 2 carpetas: 

1. La primera carpeta contiene imágenes referenciales del programa.
    ![imagen1](https://framagit.org/Marcelo/torrepokemon/-/raw/master/Im%C3%A1genes/TorrePok%C3%A9mon.PNG)
2. La segunda carpeta contiene los archivos correspondientes al programa:
    

- build
- nbproject
- src/org/jmtipane
- test
- build.xml
- manifest.mf