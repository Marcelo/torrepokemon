/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmtipane;

import javax.media.opengl.GL;
import static javax.media.opengl.GL.GL_BLEND;
import static javax.media.opengl.GL.GL_ONE_MINUS_SRC_ALPHA;
import static javax.media.opengl.GL.GL_SRC_ALPHA;
import static org.jmtipane.ProyectoFinal.gl;
import static org.jmtipane.ProyectoFinal.glu;
import static org.jmtipane.ProyectoFinal.quad;

/**
 * Proyecto_Final Torre_Pok�mon
 * @author Marcelo Tip�n
 * Fecha: 28/09/2020
 * Clase que crea las estrellas alrededor del mapa
 */
public class Estrellas {
    //Variables de posici�n
    float  x1, x2;
    //vector para almacenar estrellas
    static float star[][]= new float[100][2];
    /**
     * Constructor que genera estrellas en posiciones aleatorias
     * @param ang 
     */
    public Estrellas(float ang){
    gl.glPushMatrix();
    for(int j=0; j<=360 ;j+=90){
        gl.glRotatef(j, 0, 1, 0);
        if(ang%4 == 0 || ang <1){
            for (int i = 0; i < 100; i++) {
                x1=(float) Math.random()*3500-1500;
                x2=(float) Math.random()*1500-300;
                star[i][0]=x1;
                star[i][1]=x2;
                Estrella(x1, x2, -1000);
            }
        }
        else{
            for (int i = 0; i < 100; i++) {
                x1=star[i][0];
                x2=star[i][1];
                Estrella(x1, x2, -1000);
            }
        }
    }
    gl.glPopMatrix();
    }
    /**
     * Se define la forma de las estrellas en una posici�n espec�fica
     * @param x
     * @param y
     * @param z 
     */
    public void Estrella(float x, float y, float z){
        gl.glPushMatrix();
        gl.glTranslatef(x, y, z);
            gl.glEnable(GL_BLEND);
            gl.glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
            gl.glColor4f(1, 1, (float) Math.random(), 0.2f);
            gl.glBegin(GL.GL_LINES);
            gl.glVertex3f(0, 3, 0);
            gl.glVertex3f(0, -3, 0);
            gl.glVertex3f(3, 0, 0);
            gl.glVertex3f(-3, 0, 0);
            gl.glEnd();
            
            glu.gluSphere(quad, 1.5f, 10, 10);
//            Circulo(x, y, -2);
        gl.glPopMatrix();
    }
}
