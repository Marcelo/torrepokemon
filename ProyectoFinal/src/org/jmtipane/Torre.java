/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmtipane;

import static org.jmtipane.ProyectoFinal.gl;

/**
 * Proyecto_Final Torre_Pok�mon
 * @author Marcelo Tip�n
 * Fecha: 28/09/2020
 * Clase que dibuja todos los elementos de la torre
 */
public class Torre {
    //Variables para dibujar 
    Forma form = new Forma();
    Piso floor = new Piso();
    /**
     * Constructor que llama a todos los elementos de la torre
     */
    public Torre() {
        gl.glPushMatrix();
        gl.glTranslatef(0, -400, 0);
    //Dibuja todos los pisos
        floor.piso1();
        floor.piso2();
        floor.piso3();
        floor.piso4();
        floor.piso5();
    //Dibuja el suelo exterior
        form.suelo();
    //Dibuja el final de la torre
        form.FinTorre();
        gl.glScalef(1, 1, 1.2f);
    //Dibuja la base y detalles
        form.baseTorre();
        gl.glPopMatrix();
    }
        
    
}
