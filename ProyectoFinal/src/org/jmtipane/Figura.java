/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmtipane;

import com.sun.opengl.util.GLUT;
import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;
import static org.jmtipane.ProyectoFinal.gl;
import static org.jmtipane.ProyectoFinal.glu;
import static org.jmtipane.ProyectoFinal.quad;
import static org.jmtipane.ProyectoFinal.glut;

/**
 * Proyecto_Final Torre_Pok�mon
 * @author Marcelo Tip�n
 * Fecha: 28/09/2020
 * Clase que crea las distintas figuras b�sicas
 */
public class Figura {
    /**
     * Constructor que instancia formas glut y glu
     * para luego dibujar distintas figuras
     */
    public Figura() {
        quad = glu.gluNewQuadric();
        glu.gluQuadricDrawStyle(quad, GLU.GLU_FILL);
        glut = new GLUT();
    }
    
    /**
     * Dibuja las paredes para cada piso
     */
    public void Paredes(){
        gl.glPushMatrix();
        gl.glBegin(GL.GL_POLYGON);
//        gl.glColor3f(0.3f, 0.1f, 0.1f);
        gl.glColor3f(0.274f, 0.329f, 0.274f);
        gl.glVertex3f(-200,  0, 250);
        gl.glVertex3f( 200,  0, 250);
        gl.glVertex3f( 175, 150, 225);
        gl.glVertex3f(-175, 150, 225);
        gl.glEnd();
        
        gl.glBegin(GL.GL_POLYGON);
        gl.glColor3f(0.219f, 0.262f, 0.219f);
        gl.glVertex3f( 200,   0, 250);
        gl.glVertex3f( 250,   0, 200);
        gl.glVertex3f( 225, 150, 175);
        gl.glVertex3f( 175, 150, 225);
        gl.glEnd();  
        gl.glPopMatrix();
    }
    
    /**
     * Dibuja las paredes exteriores
     */
    public void ParedesExt(){
        gl.glPushMatrix();
        
        gl.glBegin(GL.GL_POLYGON);
        gl.glColor3f(0.098f, 0.098f, 0.098f);   //gris
//        gl.glColor3f(0.3f, 0.1f, 0.1f);   //rojo
//        gl.glColor3f(0.180f, 0.090f, 0.2f);   //morado
        gl.glVertex3f(-200,  0, 250);
        gl.glVertex3f( 200,  0, 250);
        gl.glVertex3f( 175, 150, 225);
        gl.glVertex3f(-175, 150, 225);
        gl.glEnd();
        
        gl.glBegin(GL.GL_POLYGON);
        gl.glColor3f(0.098f, 0.098f, 0.098f);   //gris
//        gl.glColor3f(0.3f, 0.1f, 0.1f);   //rojo
//        gl.glColor3f(0.180f, 0.090f, 0.2f);   //morado
        gl.glVertex3f( 200,   0, 250);
        gl.glVertex3f( 250,   0, 200);
        gl.glVertex3f( 225, 150, 175);
        gl.glVertex3f( 175, 150, 225);
        gl.glEnd();  
        gl.glPopMatrix();
    }
    
    /**
     * Dibuja las ventanas en una cara
     */
    public void ventanas(){
        gl.glPushMatrix();
        for (int i = 0; i < 5; i++) {
            gl.glBegin(GL.GL_POLYGON);
            gl.glColor3f(0, 0, 0);
            gl.glVertex3f(-125+(i*60),  61.5f, 240.5f);
            gl.glVertex3f(-100+(i*60),  61.5f, 240.5f);
            gl.glVertex3f(-100+(i*60), 101.5f, 233.5f);
            gl.glVertex3f(-125+(i*60), 101.5f, 233.5f);
            gl.glEnd();
            Circulo(-112.5f+(i*60), 100, 233.33f, 12.5f);
        }
        gl.glPopMatrix();
    }
    
    /**
     * Dibuja un segmento la base de la Torre
     */
    public void BaseTorre(){
            gl.glColor3f(0, 0, 0);
            gl.glBegin(GL.GL_POLYGON);
            gl.glVertex3f( 350,  0,  350);
            gl.glVertex3f(-350,  0,  350);
            gl.glVertex3f(-350,-20,  350);
            gl.glVertex3f( 350,-20,  350);
            gl.glEnd();
    }
    
    /**
     * Dibuja el camino exterior de la torre
     */
    public void CaminoExt(){
        gl.glPushMatrix();
//            gl.glColor3f(0.415f, 0.364f, 0.301f);   //gris
            gl.glColor3f(1.0f, 0.8f, 0.72f);
            gl.glBegin(GL.GL_POLYGON);
            gl.glVertex3f( -750, -418,  -750);
            gl.glVertex3f( -600, -418,  -750);
            gl.glVertex3f( -600, -418,   750);
            gl.glVertex3f( -750, -418,   750);
            gl.glEnd();
            
            for(int i=0; i<80;i+=3){
                gl.glColor3f(0.098f, 0.098f, 0.098f);   
                gl.glBegin(GL.GL_POLYGON);
                gl.glVertex3f( -800, -418,  -800+(i*20));
                gl.glVertex3f( -800, -418,  -770+(i*20));
                gl.glVertex3f( -800, -368,  -770+(i*20));
                gl.glVertex3f( -800, -348,  -785+(i*20));
                gl.glVertex3f( -800, -368,  -800+(i*20));
                gl.glEnd();
            }
        gl.glPopMatrix();
    }
    
    /**
     * Dibuja el camino central de la torre
     */
    public void CaminoExt2(){
        gl.glPushMatrix();
//            gl.glColor3f(0.415f, 0.364f, 0.301f);   //gris
            gl.glColor3f(1.0f, 0.8f, 0.72f);
            gl.glBegin(GL.GL_POLYGON);
            gl.glVertex3f( -80, -418,   0);
            gl.glVertex3f(  80, -418,   0);
            gl.glVertex3f(  80, -418, 750);
            gl.glVertex3f( -80, -418, 750);
            gl.glEnd();
        gl.glPopMatrix();
    }
    
    /**
     * Dibuja el Piso y el techo para cada piso
     */    
    public void TechoPiso(){
        gl.glBegin(GL.GL_POLYGON);
        gl.glColor3f(0.223f, 0.223f, 0.223f);
        gl.glVertex3f(-200,  0, 250);
        gl.glVertex3f( 200,  0, 250);
        gl.glVertex3f( 250,  0, 200);
        gl.glVertex3f( 250,  0,-200);
        gl.glVertex3f( 200,  0,-250);
        gl.glVertex3f(-200,  0,-250);
        gl.glVertex3f(-250,  0,-200);
        gl.glVertex3f(-250,  0, 200);
        gl.glEnd();
        
        gl.glBegin(GL.GL_POLYGON);
        gl.glColor3f(0.0f, 0.0f, 0.0f);
        gl.glVertex3f(-175, 150, 225);
        gl.glVertex3f( 175, 150, 225);
        gl.glVertex3f( 225, 150, 175);
        gl.glVertex3f( 225, 150,-175);
        gl.glVertex3f( 175, 150,-225);
        gl.glVertex3f(-175, 150,-225);
        gl.glVertex3f(-225, 150,-175);
        gl.glVertex3f(-225, 150, 175);
        gl.glEnd();
    }
    
    /**
     * Dibuja los rect�ngulos que forman las gradas
     */
    public void rectangulo(){
        
        gl.glPushMatrix();
        //lineas 
            gl.glColor3f(0.01f, 0.01f, 0.01f);           
            gl.glBegin(GL.GL_LINES);
            gl.glVertex3f(-50, 10.5f,   -9.5f);
            gl.glVertex3f( 50, 10.5f,   -9.5f);
            gl.glEnd();
        
        gl.glColor3f(0.6f, 0.6f, 0.6f);
        for(int i=0; i<=360 ;i+=90){
            gl.glRotatef(i, 1, 0, 0);
            gl.glBegin(GL.GL_POLYGON);
            gl.glVertex3f(-50, -10,  -10);
            gl.glVertex3f(-50, -10,   10);
            gl.glVertex3f( 50, -10,   10);
            gl.glVertex3f( 50, -10,  -10);
            gl.glEnd();
        }
        gl.glPopMatrix();
        
    }
    
    /**
     * Crea una circunferencia 
     * @param posx
     * @param posy
     * @param posz
     * @param radio 
     */
    public void Circulo(float posx, float posy, float posz, float radio){
        float x,y;
        gl.glBegin(GL.GL_POLYGON);
        for(float i=0; i<10;i+=0.01){
                x=(float) (radio *Math.cos(i));
                y=(float) (radio *Math.sin(i));
                gl.glVertex3f(x+posx, y+posy, posz);
            }
        gl.glEnd();
    }
    
    /**
     * Otro metodo para dibujar una circunferencia 
     * @param posx
     * @param posy
     * @param posz
     * @param radio 
     */
    public void Circunferencia(float posx, float posy, float posz, float radio){
        float Pi = 3.14159265358979323846f;
        float x,y;
        gl.glBegin(GL.GL_POLYGON);
        for(float i=0; i<100;i+=0.01){
                x=(float) (radio *Math.cos(i*2*Pi/100));
                y=(float) (radio *Math.sin(i*2*Pi/100));
                gl.glVertex3f(x+posx, y+posy, posz);
            }
        gl.glEnd();
    }
    
    /**
     * Dibuja una semicircunferencia
     * @param posx
     * @param posy
     * @param posz
     * @param radio 
     */
    public void semiCircunferencia(float posx, float posy, float posz, float radio){
        float Pi = 3.14159265358979323846f;
        gl.glBegin(GL.GL_POLYGON);
            for(int i=0; i<51; i++){
               float x = (float) (radio * Math.cos(i*2*Pi/100));
               float y = (float) (radio * Math.sin(i*2*Pi/100));
               gl.glVertex3f(x+posx, y+posy, posz); 
            }
        gl.glEnd();
    }
    
    /**
     * Dibuja un cuarto de la circunferencia
     * @param posx
     * @param posy
     * @param posz
     * @param radio 
     */
    public void cuartoCircunferencia(float posx, float posy, float posz, float radio){
        float Pi = 3.14159265358979323846f;
        gl.glBegin(GL.GL_POLYGON);
            gl.glVertex3f(posx, posy, posz); 
            for(int i=0; i<26; i++){
               float x = (float) (radio * Math.cos(i*2*Pi/100));
               float y = (float) (radio * Math.sin(i*2*Pi/100));
               gl.glVertex3f(x+posx, y+posy, posz); 
            }
        gl.glEnd();
    }
    /**
     * Dibuja un tri�ngulo
     * @param posx
     * @param posy
     * @param posz 
     */
    public void triangulo(float posx, float posy, float posz){
        float Pi = 3.14159265358979323846f;
        gl.glBegin(GL.GL_POLYGON);
        gl.glVertex3f(posx, posy, posz); 
        gl.glVertex3f(posx+6, posy, posz);   
        gl.glVertex3f(posx+3, posy-10, posz); 
        gl.glEnd();
    }
}
