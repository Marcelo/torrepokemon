/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmtipane;

import static org.jmtipane.ProyectoFinal.gl;

/**
 * Proyecto_Final Torre_Pok�mon
 * @author Marcelo Tip�n
 * Fecha: 28/09/2020
 * Dibuja cada piso de la torre
 */
public class Piso {
    //Variavle para dibujar distintas formas
    Forma form=new Forma();
    /**
     * Constructor por defecto
     */
    public Piso() {
    }
    /**
     * Dibuja el primer piso
     */
    public void piso1(){
        gl.glPushMatrix();
    //Detalles interiores y exteriores
        gl.glTranslatef(0, 1, 0);
        form.piso();
        form.ventanas1();
        gl.glTranslatef(0, 0, -75);
        gl.glRotatef(180, 0, 1, 0);
        form.Puerta();
        form.PuertaInt();
        gl.glTranslatef(0, 0, -150);
        gl.glRotatef(180, 0, 1, 0);
        form.gradas();
    //Pilares
        gl.glTranslatef(0, 400, -80);
        form.pilarInt(-150, 0,  150);
        form.pilarInt(-150, 0, -150);
        form.pilarInt( 150, 0,  150);
        form.pilarInt( 150, 0, -150);
    //Tumbas
        gl.glTranslatef(0, -75, 0);
        gl.glScalef(1.5f, 1.2f, 1.8f);
        form.lapidaL( -30, 0 , -120,-90);
        form.lapidaL(  30, 0 , -120,-90);
        gl.glRotatef(180, 0, 1, 0);
        form.lapidaL(-140, 0 ,  -30, 0);
        form.lapidaL(-140, 0 ,   30, 0);
        
        gl.glPopMatrix();
    }
    /**
     * Dibuja el segundo piso
     */
    public void piso2(){
        gl.glPushMatrix();
    //Detalles interiores y exteriores
        gl.glScalef(0.88f, 0.95f, 0.88f);
        gl.glTranslatef(0, 159, 0);
        form.piso();
        form.ventanas();
        gl.glTranslatef(0, 0, -75);
        gl.glRotatef(-180, 0, 1, 0);
        form.gradas();
        form.borde();
    //Pilares
        gl.glTranslatef(0, 400, -80);
        form.pilarInt(-150, 0,  150);
        form.pilarInt(-150, 0, -150);
        form.pilarInt( 150, 0,  150);
        form.pilarInt( 150, 0, -150);
    //Tumbas
        gl.glTranslatef(0, -80, 0);
        gl.glScalef(1.5f, 1.18f, 2.0f);
        form.lapidaL( 140, 0 ,  -30, 180);
        form.lapidaL( 140, 0 ,   30,-180);
        form.lapidaL(-140, 0 ,  -30,   0);
        form.lapidaL(-140, 0 ,   30,   0);
        gl.glPopMatrix();
    }
    /**
     * Dibuja el tercer piso
     */
    public void piso3(){
        gl.glPushMatrix();
    //Detalles interiores y exteriores
        gl.glScalef(0.77f, 0.9f, 0.77f);
        gl.glTranslatef(0, 327, 0);
        form.piso();
        form.ventanas();
        gl.glTranslatef(0, 0, 75);
        form.gradas();
        form.borde();
    //Pilares
        gl.glTranslatef(0, 400, -80);
        form.pilarInt(-150, 0,  150);
        form.pilarInt(-150, 0, -150);
        form.pilarInt( 150, 0,  150);
        form.pilarInt( 150, 0, -150);
    //Tumbas
        gl.glTranslatef(0, -85, 0);
        gl.glScalef(1.5f, 1.16f, 2.2f);
        form.lapidaL( 140, 0 ,  -30, 180);
        form.lapidaL( 140, 0 ,   30,-180);
        form.lapidaL(-140, 0 ,  -30,   0);
        form.lapidaL(-140, 0 ,   30,   0);
        gl.glPopMatrix();
    }
    /**
     * Dibuja el cuarto piso
     */
    public void piso4(){
        gl.glPushMatrix();
    //Detalles interiores y exteriores
        gl.glScalef(0.67f, 0.85f, 0.67f);
        gl.glTranslatef(0, 505.5f, 0);
        form.piso();
        form.ventanas();
        gl.glTranslatef(0, 0, -75);
        gl.glRotatef(180, 0, 1, 0);
        form.gradas();
        form.borde();
    //Pilares
        gl.glTranslatef(0, 400, -80);
        form.pilarInt(-150, 0,  150);
        form.pilarInt(-150, 0, -150);
        form.pilarInt( 150, 0,  150);
        form.pilarInt( 150, 0, -150);
    //Tumbas
        gl.glTranslatef(0, -92, 0);
        gl.glScalef(1.5f, 1.14f, 2.4f);
        form.lapidaL( 140, 0 ,  -30, 180);
        form.lapidaL( 140, 0 ,   30,-180);
        form.lapidaL(-140, 0 ,  -30,   0);
        form.lapidaL(-140, 0 ,   30,   0);
        gl.glPopMatrix();
    }
    /**
     * Dibuja el quinto piso
     */
    public void piso5(){
        gl.glPushMatrix();
    //Detalles interiores y exteriores
        gl.glScalef(0.58f, 0.80f, 0.58f);
        gl.glTranslatef(0, 697, 0);
        form.piso();
        form.ventanas();
        gl.glTranslatef(0, 0, 75);
        form.borde();
    //Pilares
        gl.glTranslatef(0, 400, -80);
        form.pilarInt(-150, 0,  150);
        form.pilarInt(-150, 0, -150);
        form.pilarInt( 150, 0,  150);
        form.pilarInt( 150, 0, -150);
    //Tumbas
        gl.glTranslatef(0, -97, 0);
        gl.glScalef(1.5f, 1.12f, 2.6f);
        form.lapidaL( 140, 0 ,  -30, 180);
        form.lapidaL( 140, 0 ,   30,-180);
        form.lapidaL(-140, 0 ,  -30,   0);
        form.lapidaL(-140, 0 ,   30,   0);
        gl.glScalef(2.2f, 1.6f, 1.0f);
        form.lapidaL( 0, 101 , 80, 90);
        gl.glPopMatrix();
    }
    
}
