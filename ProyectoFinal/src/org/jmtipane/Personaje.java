/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmtipane;

import javax.media.opengl.glu.GLUquadric;
import static org.jmtipane.ProyectoFinal.gl;
import static org.jmtipane.ProyectoFinal.glu;

/**
 * Proyecto_Final Torre_Pok�mon
 * @author Marcelo Tip�n
 * Fecha: 28/09/2020
 * Clase para dibujar un personaje, Gastly de Pok�mon
 */
public final class Personaje {
    //Variables 
    Figura fig = new Figura();
    Camara cam = new Camara();
    static GLUquadric quad = glu.gluNewQuadric();
    /**
     * Constructor que define posici�n del personaje
     * @param ang
     * @param radio
     * @param alto 
     */
    public Personaje(float ang, float radio, float alto){
        gl.glPushMatrix();
    //Movimiento libre del personaje
        if(cam.getRot()==0 || cam.getRot()==1 || cam.getRot()==3){
            gl.glRotatef(ang, 0.0f, 1.0f, 0.0f);
            gl.glTranslatef(0, (float) (alto + 55*(Math.sin(ang/30))), radio);
            gl.glRotatef(90, 0, 1, 0);
        }
    //Movimiento por teclado del personaje
        if(cam.getRot()==2){
            gl.glRotatef(cam.getRx(), 1.0f, 0.0f, 0.0f);
            gl.glRotatef(cam.getRy(), 0.0f, 1.0f, 0.0f);
            gl.glTranslatef(cam.getTx(), cam.getTy()-40, cam.getTz()-150);
            gl.glRotatef(180, 0, 1, 0);
        }
    //Crea el personaje
        cuerpo();
        ojoD();
        ojoI();
        bocaI();
        bocaD();
        colmilloD();
        colmilloI();
        Detalles();
        gl.glPopMatrix();
    }
    /**
     * Dibuja el cuerpo del personaje
     */
    public void cuerpo(){
        gl.glPushMatrix();
        gl.glColor3f(0.0f, 0.0f, 0.0f);
        glu.gluSphere(quad, 25, 30, 30);
        gl.glPopMatrix();  
    }
    /**
     * Dibuja el ojo derecho del personaje
     */
    public void ojoD(){
        //ojo derecho
        gl.glPushMatrix();
        gl.glColor3f(1.0f, 1.0f, 1.0f);
        gl.glTranslatef(8, 10, 9);
        gl.glRotatef(225, 0, 0, 1);
        gl.glRotatef(-40, 0, 1, 0);
        gl.glScalef(1, 1.1f, 1);
        fig.semiCircunferencia(0, 0,  10, 13);
        //pupila derecha
        gl.glColor3f(0, 0, 0);
        fig.Circulo(10, 5, 11, 1.5f);
        gl.glPopMatrix();
    }
    /**
     * Dibuja el ojo izquierdo del personaje
     */
    public void ojoI(){
        //ojo izquierdo
        gl.glPushMatrix();
        gl.glColor3f(1.0f, 1.0f, 1.0f);
        gl.glTranslatef(-8, 10, 9);
        gl.glRotatef(135, 0, 0, 1);
        gl.glRotatef(40, 0, 1, 0);
        gl.glScalef(1, 1.1f, 1);
        fig.semiCircunferencia(0, 0,  10, 13);
        //pupila derecha
        gl.glColor3f(0, 0, 0);
        fig.Circulo(-10, 5, 11, 1.5f);
        gl.glPopMatrix();
    }
    //Dibuja la parte izquierda de la boca
    public void bocaI(){
        gl.glPushMatrix();
        gl.glColor3f(0.7490f, 0.286f, 0.262f);
        gl.glTranslatef(2, 2, 18);
        gl.glRotatef(180, 0, 0, 1);
        gl.glRotatef(-25, 1, 0, 0);
        gl.glRotatef(10, 0, 1, 0);
        gl.glScalef(1.3f, 0.4f, 1);
        fig.cuartoCircunferencia(0, 0,  10, 15);
        gl.glPopMatrix();
    }
    /**
     * Dibuja la parde derecha de la boca
     */
    public void bocaD(){
        gl.glPushMatrix();
        gl.glColor3f(0.7490f, 0.286f, 0.262f);
        gl.glTranslatef(2, -6, 36);
        gl.glRotatef(180, 0, 0, 1);
        gl.glRotatef(-25, 1, 0, 0);
        gl.glRotatef(-190, 0, 1, 0);
        gl.glScalef(1.3f, 0.4f, 1);
        fig.cuartoCircunferencia(0, 0,  10, 15);
        gl.glPopMatrix();
    }
    /**
     * Dibuja el colmillo derecho
     */
    public void colmilloD(){
        gl.glPushMatrix();
        gl.glColor3f(1.0f, 1.0f, 1.0f);
        gl.glTranslatef(-7, -3f, 8);
        gl.glRotatef(10, 0, 1, 0);
        gl.glRotatef(6, 0, 0, 1);
        fig.triangulo(15, 0, 20);
        gl.glPopMatrix();
    }
    /**
     * Dibuja el colmillo izquierdo
     */
    public void colmilloI(){
        gl.glPushMatrix();
        gl.glColor3f(1.0f, 1.0f, 1.0f);
        gl.glTranslatef(2, -3f, 8);
        gl.glRotatef(-10, 0, 1, 0);
        gl.glRotatef(-6, 0, 0, 1);
        fig.triangulo(-15, 0, 20);
        gl.glPopMatrix();
    }
    /**
     * Dibuja los detalles de las nubes que siguen al personaje
     */
    public void Detalles(){
        gl.glPushMatrix();
        
        gl.glColor4f(0.482f, 0.129f, 0.505f, 0.7f);
        gl.glTranslatef(0, 2, -25);
        glu.gluSphere(quad, 12, 30, 30);
        
        gl.glTranslatef(0, -4, -25);
        glu.gluSphere(quad, 8, 30, 30);
        
        gl.glTranslatef(0, -8, -25);
        glu.gluSphere(quad, 4, 30, 30);
        
        gl.glTranslatef(0, -10, -25);
        glu.gluSphere(quad, 0.1f, 30, 30);
        
        gl.glTranslatef(6, 20, 30);
        glu.gluSphere(quad, 6, 30, 30);
        
        gl.glTranslatef(3, 15, 45);
        glu.gluSphere(quad, 7, 30, 30);
//        
        gl.glTranslatef(-15, -25, -15);
        glu.gluSphere(quad, 8, 30, 30);
//        
        gl.glTranslatef(-5, 0, 10);
        glu.gluSphere(quad, 8, 30, 30);
//        
        gl.glTranslatef(3, -15, 5);
        glu.gluSphere(quad, 1, 30, 30);
        
        gl.glTranslatef(-5, 15, 15);
        glu.gluSphere(quad, 2, 30, 30);
//        
        gl.glTranslatef(6, 10, -30);
        glu.gluSphere(quad, 6, 30, 30);
        
        gl.glTranslatef(3, 25, 5);
        glu.gluSphere(quad, 6, 30, 30);
        
        gl.glColor4f(0.482f, 0.129f, 0.505f, 0.4f);
        gl.glTranslatef(-15, -25, 15);
        glu.gluSphere(quad, 9, 30, 30);
        
        gl.glTranslatef(16, 10, -25);
        glu.gluSphere(quad, 6, 30, 30);
//        
        gl.glTranslatef(3, 0, -45);
        glu.gluSphere(quad, 2, 30, 30);
        
        gl.glTranslatef(15, -15, -5);
        glu.gluSphere(quad, 1, 30, 30);
        
        gl.glTranslatef(6, -2, -1);
        glu.gluSphere(quad, 2, 30, 30);
//        
        gl.glTranslatef(-30, 0, 5);
        glu.gluSphere(quad, 2, 30, 30);
        
        gl.glTranslatef(-1, 0, 15);
        glu.gluSphere(quad, 1, 30, 30);
        gl.glPopMatrix();  
    }
}
