/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmtipane;

import static org.jmtipane.ProyectoFinal.gl;
/**
 * Proyecto_Final Torre_Pok�mon
 * @author Marcelo Tip�n
 * Fecha: 28/09/2020
 * Clase que dibuja los elementos del exterior de la torre
 */
public class Exterior {
    //Variable para llamar distintas formas
    Forma form = new Forma();
    
    /**
     * Constructor para crear el exterior
     */
    public Exterior(){
        arboles();
        form.paredExt();
        form.caminos();
        form.pilar(-300, 0,  340);
        form.pilar(-300, 0, -340);
        form.pilar(-550, -20,  150);
        form.pilar(-550, -20, -150);
        gl.glRotatef(180, 0, 1, 0);
        form.PuertaExtInt();
        form.PuertaExt();
    }
    
    /**
     * Dibuja las filas de arboles
     */
    public void arboles(){
        gl.glPushMatrix();
        for (int k = 0; k < 360; k+=90) {
            gl.glRotatef(k, 0, -1, 0);
            for (int i = 0; i < 1; i++) {
                gl.glTranslatef(0, 0, 150);
                for (int j = 0; j < 10; j++) {
                    gl.glTranslatef(200, 0, 0);
                    form.arbol();
                }
                gl.glTranslatef(-2000, 0, -200);
            }
        }
        gl.glPopMatrix();
    }
}
