/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmtipane;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JFrame;
import static org.jmtipane.ProyectoFinal.gl;

/**
 * Proyecto_Final Torre_Pok�mon
 * @author Marcelo Tip�n
 * Fecha: 28/09/2020
 * Clase que controla las c�maras
 */
public class Camara extends JFrame implements KeyListener {

//    //Variables para rotaci�n y traslaci�n
    private static float   rx, ry, rz,
                           tx, ty, tz;
    
    //Variable para indicar la c�mara, default 0
    static int cam;
    
    //Variables de estado para rotaci�n
    static float rot;
    static boolean aux=true;
    
    /**
     * Constructor por defecto
     */
    public Camara(){
    }
    
    /**
     * Constructor con el par�metro de c�mara que se mostrar�
     * @param camara 
     */
    public Camara(int camara){
        cam=camara;
            tx=  0;
            ty=500;
            tz=700;
            rx=  0;
            ry=  0;
            rz=-10;
    }
    
    //Get de los valores de rotaci�n y traslaci�n
    public float getRx() {
        return rx;
    }

    public float getRy() {
        return ry;
    }

    public float getRz() {
        return rz;
    }

    public float getTx() {
        return tx;
    }

    public float getTy() {
        return ty;
    }

    public float getTz() {
        return tz;
    }

    public static int getCam() {
        return cam;
    }

    public static float getRot() {
        return rot;
    }

    public static void setTx(float tx) {
        Camara.tx = tx;
    }

    public static void setTz(float tz) {
        Camara.tz = tz;
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }
    //Control por teclado
    @Override
    public void keyPressed(KeyEvent e) {
    //Indica que c�mara se ha seleccionado y define una posici�n por default
        if(e.getKeyCode() == KeyEvent.VK_1){
            cam=0;
        }
        if(e.getKeyCode() == KeyEvent.VK_2){
            cam=1;
        }
        if(e.getKeyCode() == KeyEvent.VK_3){
            cam=2;
            rot = 0;
                tx= 0; ty=-300; tz= 950;
                rx= 0; ry= -90; rz= 0;
        }
        if(e.getKeyCode() == KeyEvent.VK_4){
            cam=3;
            ProyectoFinal.aux=500;
            ProyectoFinal.ang=0;
            rx=0; ry=0; rz=-1;
            tx=0; ty=-100; tz=2000;
        }
        if(e.getKeyCode() == KeyEvent.VK_5){
            cam=4;
            rx=0; ry=0; rz=-10;
            tx=0; ty=50; tz=1500;
        }
        if(e.getKeyCode() == KeyEvent.VK_6){
            cam=5;
            rot = 0;
                rx=12; ry=-1; rz=-10;
                tx=-1200; ty=-45; tz=1000;
        }
    //Reset C�mara
        if(e.getKeyCode() == KeyEvent.VK_7){
            rx=0; ry=0; rz=-10;
            tx=0; ty=50; tz=1000;
        }
    //Reset View
        if(e.getKeyCode() == KeyEvent.VK_8){
            rx=0; ry=0; rz=-10;
        }
    //Indica que sucede al seleccionar cada c�mara
        switch(cam){
            //C�mara fija
            case 0:
                rot = 0;
                tx=-1800;
                ty= 800;
                tz= -900;
                rx= 1500;
                ry=-900;
                rz= 800;
            break;
            //C�mara sigue a la luna
            case 1:
                rot = 1;
                tx=-1600;
                ty= -300;
                tz= 100;
                rx= 1800;
                ry= 400;
                rz= 0;
            break;
            //C�mara sigue al personaje
            case 2:
                rot = 2;
                //Traslada al personaje
                if(e.getKeyCode() == KeyEvent.VK_RIGHT){
                tx+=10f;
                }
                if(e.getKeyCode() == KeyEvent.VK_LEFT){
                tx-=10f;
                }
                if(e.getKeyCode() == KeyEvent.VK_UP){
                ty-=10f;
                }
                if(e.getKeyCode() == KeyEvent.VK_DOWN){
                ty+=10f;
                }
                if(e.getKeyCode() == KeyEvent.VK_Z){
                tz-=10f;
                }
                if(e.getKeyCode() == KeyEvent.VK_X){
                tz+=10f;
                }
                //Rota al personaje hacia la derecha
                if(e.getKeyCode() == KeyEvent.VK_L){
                    gl.glRotatef(ry, 0, 1, 0);
                    ry+=5;
                }
                //Rota al personaje hacia la izquierda
                if(e.getKeyCode() == KeyEvent.VK_J){
                    gl.glRotatef(ry, 0, 1, 0);
                    ry-=5;
                }
                
                //Rota al personaje hacia la Arriba
                if(e.getKeyCode() == KeyEvent.VK_I){
                    gl.glRotatef(rx, 1, 0, 0);
                    rx+=5;
                }
                //Rota al personaje hacia la abajo
                if(e.getKeyCode() == KeyEvent.VK_K){
                    gl.glRotatef(rx, 1, 0, 0);
                    rx-=5;
                }
                
            break;
            
            //C�mara con trayectoria eliptica
            case 3:
                rot = 3;;
            break;
            
            //C�mara con trayectoria circular variable
            case 4:
                rot = 1;
                if(e.getKeyCode() == KeyEvent.VK_RIGHT){
                tz+=10f;
                tx+=10f;
                }
                if(e.getKeyCode() == KeyEvent.VK_LEFT){
                tz-=10f;
                tx-=10f;
                }
                ty= 300;
                rx= -tx;
                ry= 10-ty;
                rz= -10-tz;
            break;
            //C�mara con movimiento libre
            case 5:
                rot = 0;
                //Traslada al personaje
                if(e.getKeyCode() == KeyEvent.VK_RIGHT){
                tx+=10f;
                }
                if(e.getKeyCode() == KeyEvent.VK_LEFT){
                tx-=10f;
                }
                if(e.getKeyCode() == KeyEvent.VK_UP){
                ty-=10f;
                }
                if(e.getKeyCode() == KeyEvent.VK_DOWN){
                ty+=10f;
                }
                if(e.getKeyCode() == KeyEvent.VK_Z){
                tz-=10f;
                }
                if(e.getKeyCode() == KeyEvent.VK_X){
                tz+=10f;
                }
                //Rota al personaje hacia la derecha
                if(e.getKeyCode() == KeyEvent.VK_L){
                    if(rz <= 0){
                        if(rx<=10 && rx>0){
                            rz+=1;
                            rx+=1;
                        }
                        if(rx>=-10 && rx<0){
                            rz-=1;
                            rx+=1;
                        }
                        if(rx==0){
                            rz+=1;
                            rx+=1;
                        }
                        if(rx==10){
                            rz+=1;
                            rx-=1;
                        }
                    }
                    if(rz > 0){
                        if(rx<=10 && rx>0){
                            rz+=1;
                            rx-=1;
                        }
                        if(rx>=-10 && rx<0){
                            rz-=1;
                            rx-=1;
                        }
                        if(rx==0){
                            rz-=1;
                            rx-=1;
                        }
                        if(rx==-10){
                            rz-=1;
                            rx+=1;
                        }
                    }
                }
                //Rota al personaje hacia la izquierda
                if(e.getKeyCode() == KeyEvent.VK_J){
                    if(rz <= 0){
                        if(rx<=10 && rx>0){
                            rz-=1;
                            rx-=1;
                        }
                        if(rx>=-10 && rx<0){
                            rz+=1;
                            rx-=1;
                        }
                        if(rx==0){
                            rz+=1;
                            rx-=1;
                        }
                        if(rx==10){
                            rz-=1;
                            rx-=1;
                        }
                        if(rx==-10){
                            rz+=1;
                            rx+=1;
                        }
                    }
                    if(rz > 0){
                        if(rx<=10 && rx>0){
                            rz-=1;
                            rx+=1;
                        }
                        if(rx>=-10 && rx<0){
                            rz+=1;
                            rx+=1;
                        }
                        if(rx==0){
                            rz-=1;
                            rx+=1;
                        }
                        
                    }
                }
                
                //Rota al personaje hacia la Arriba
                if(e.getKeyCode() == KeyEvent.VK_I){
                    if(ry <= 0){
                        if(rz<=10 && rz>0){
                            ry+=1;
                            rz+=1;
                        }
                        if(rz>=-10 && rz<0){
                            ry-=1;
                            rz+=1;
                        }
                        if(rz==0){
                            ry+=1;
                            rz+=1;
                        }
                        if(rz==10){
                            ry+=1;
                            rz-=1;
                        }
                    }
                    if(ry > 0){
                        if(rz<=10 && rz>0){
                            ry+=1;
                            rz-=1;
                        }
                        if(rz>=-10 && rz<0){
                            ry-=1;
                            rz-=1;
                        }
                        if(rz==0){
                            ry-=1;
                            rz-=1;
                        }
                        if(rz==-10){
                            ry-=1;
                            rz+=1;
                        }
                    }
                }
                //Rota al personaje hacia la abajo
                if(e.getKeyCode() == KeyEvent.VK_K){
                    if(ry <= 0){
                        if(rz<=10 && rz>0){
                            ry-=1;
                            rz-=1;
                        }
                        if(rz>=-10 && rz<0){
                            ry+=1;
                            rz-=1;
                        }
                        if(rz==0){
                            ry+=1;
                            rz-=1;
                        }
                        if(rz==10){
                            ry-=1;
                            rz-=1;
                        }
                        if(rz==-10){
                            ry+=1;
                            rz+=1;
                        }
                    }
                    if(ry > 0){
                        if(rz<=10 && rz>0){
                            ry-=1;
                            rz+=1;
                        }
                        if(rz>=-10 && rz<0){
                            ry+=1;
                            rz+=1;
                        }
                        if(rz==0){
                            ry-=1;
                            rz+=1;
                        }
                        
                    }
                }
                
            break;
        }
    }
    @Override
    public void keyReleased(KeyEvent e) {
        
    }
    
}
