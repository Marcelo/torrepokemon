package org.jmtipane;

import com.sun.opengl.util.Animator;
import com.sun.opengl.util.GLUT;
import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import javax.media.opengl.GL;
import static javax.media.opengl.GL.GL_DONT_CARE;
import static javax.media.opengl.GL.GL_FOG;
import static javax.media.opengl.GL.GL_FOG_COLOR;
import static javax.media.opengl.GL.GL_FOG_DENSITY;
import static javax.media.opengl.GL.GL_FOG_END;
import static javax.media.opengl.GL.GL_FOG_HINT;
import static javax.media.opengl.GL.GL_FOG_MODE;
import static javax.media.opengl.GL.GL_FOG_START;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCanvas;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.glu.GLU;
import javax.media.opengl.glu.GLUquadric;



/**
 * Proyecto_Final Torre_Pok�mon
 * @author Marcelo Tip�n
 * Fecha: 28/09/2020
 * Programa que dibuja la Torre del juego Pok�mon
 */
public class ProyectoFinal implements GLEventListener {
    
    //variables de Opengl
    static GL gl;
    static GLU glu;
    static GLUT glut;
    //Forma quadric, para dibujar
    static GLUquadric quad;
    //Variables auxiliares
    static float ang=0, rot=0, aux=500;
    //Variable para definir la c�mara usada
    static Camara cam=new Camara(0);
    
    public static void main(String[] args) {
        Frame frame = new Frame("Torre Pok�mon");
        GLCanvas canvas = new GLCanvas();

        canvas.addGLEventListener(new ProyectoFinal());
        frame.add(canvas);
        frame.setSize(1500, 1500);
        final Animator animator = new Animator(canvas);
        frame.addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent e) {
                // Run this on another thread than the AWT event queue to
                // make sure the call to Animator.stop() completes before
                // exiting
                new Thread(new Runnable() {

                    public void run() {
                        animator.stop();
                        System.exit(0);
                    }
                }).start();
            }
        });
        // Center frame
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        animator.start();
        
        frame.addKeyListener(cam);
    }
    /**
     * Inicializa variables y color
     * @param drawable 
     */
    public void init(GLAutoDrawable drawable) {
        // Use debug pipeline
        // drawable.setGL(new DebugGL(drawable.getGL()));

        GL gl = drawable.getGL();
        System.err.println("INIT GL IS: " + gl.getClass().getName());

        // Enable VSync
        gl.setSwapInterval(1);

        // Setup the drawing area and shading mode
        gl.glClearColor(0.5f, 0.4f, 0.45f, 0.0f);
        gl.glShadeModel(GL.GL_SMOOTH); // try setting this to GL_FLAT and see what happens.
    }
    /**
     * Define la forma perspectiva y dimensiones de la pantalla
     * @param drawable
     * @param x
     * @param y
     * @param width
     * @param height 
     */
    public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
        GL gl = drawable.getGL();
        GLU glu = new GLU();

        if (height <= 0) { // avoid a divide by zero error!
        
            height = 1;
        }
        final float h = (float) width / (float) height;
        gl.glViewport(0, 0, width, height);
        gl.glMatrixMode(GL.GL_PROJECTION);
        gl.glLoadIdentity();
        glu.gluPerspective(45.0f, h, 1.0, 4000.0);
//        gl.glOrtho(-1000, 1000, -1000, 1000, 1000, -1000);
        gl.glMatrixMode(GL.GL_MODELVIEW);
        gl.glLoadIdentity();
    }
    /**
     * Instancia miembros de clase Glu para dibujarlos
     * Genera un contador para usarlo en c�maras y rotaciones
     * Crea la niebla del mapa
     * @param drawable 
     */
    public void display(GLAutoDrawable drawable) {
        gl = drawable.getGL();
        glu = new GLU();
        glut = new GLUT();
        quad = glu.gluNewQuadric();
        glu.gluQuadricDrawStyle(quad, GLU.GLU_FILL);
        glut = new GLUT();

        //Variable para rotaci�n
        ang=ang+0.5f;
        
        //permite usar profundidad
        gl.glEnable(GL.GL_DEPTH_TEST);
        // Clear the drawing area
        gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);
        // Reset the current matrix to the "identity"
        gl.glLoadIdentity();
        
//        glu.gluLookAt(1000, -50, 1000, 0, -20, -200, 0, 1, 0);

    //Crea niebla sobre el escenario
        //Color of the fog : grey fog
    float [] fogColor = new float[]{0.886f, 0.847f, 0.894f, 10.0f};
            ByteBuffer vbb = ByteBuffer.allocateDirect(fogColor.length*4);
        vbb.order(ByteOrder.nativeOrder());
        FloatBuffer fb = vbb.asFloatBuffer();
        fb.put(fogColor);
        fb.position(0);
    gl.glFogfv(GL_FOG_COLOR, fb);
    gl.glFogi(GL_FOG_MODE, GL.GL_EXP);
    gl.glFogf(GL_FOG_DENSITY, 0.00059f);
    gl.glFogf(GL_FOG_START, 0.0f);
    gl.glFogf(GL_FOG_END, 1.0f);
    gl.glHint(GL_FOG_HINT, GL_DONT_CARE);
    //enable the fog mode
    gl.glEnable(GL_FOG);
    //Clearing color equal to the fog color
    gl.glClearColor(fogColor[0], fogColor[1], fogColor[2], 0.0f);
    
    //Define lo que realiza cada c�mara
        //C�mara para el movimiento el�ptico
        if(cam.getRot()==3){
            gl.glRotatef(-ang*3.5f, 0, 1, 0);
            glu.gluLookAt(  cam.getTx(), cam.getTy(), cam.getTz(), 
                            cam.getTx()+cam.getRx(), cam.getTy()+cam.getRy(), cam.getTz()+cam.getRz(), 
                            0, 1, 0);
            rot=ang;
            if(aux==500 ){
                    cam.aux = true;
                }
            if(cam.aux == true){
                cam.setTz(cam.getTz()-40);
                if(aux<=500 && aux>225){
                    cam.setTx(cam.getTx()+20);
                }
                if(aux<=225 && aux>=0){
                    cam.setTx(cam.getTx()-25);
                }
//                cam.setTx(-(float) Math.sqrt((1-(Math.pow(cam.getTz(), 2)/16))-25));
                aux-=5;
            }
            if(aux==0 ){
                cam.aux = false;
            }
            if(cam.aux == false){
                cam.setTz(cam.getTz()+40);
                if(aux<=500 && aux>225){
                    cam.setTx(cam.getTx()+20);
                }
                if(aux<=225 && aux>=0){
                    cam.setTx(cam.getTx()-25);
                }
                aux+=5;
            }
        }
        //Define la pantalli fija o m�vil
        if(cam.getRot()==2){
            rot=0;
            glu.gluLookAt(  cam.getTx(), cam.getTy(), cam.getTz(), 
                            cam.getTx(), cam.getTy(), cam.getTz()-10, 
                            0, 1, 0);
            gl.glRotatef(-cam.getRy(), 0, 1, 0);
            gl.glRotatef(-cam.getRx(), 1, 0, 0);
        }
        if(cam.getRot()==1){
            glu.gluLookAt(  cam.getTx(), cam.getTy(), cam.getTz(), 
                            cam.getTx()+cam.getRx(), cam.getTy()+cam.getRy(), cam.getTz()+cam.getRz(), 
                            0, 1, 0);
            rot=ang;
            gl.glRotatef(-ang, 0, 1, 0);
        }
        if(cam.getRot()==0){
            glu.gluLookAt(  cam.getTx(), cam.getTy(), cam.getTz(), 
                            cam.getTx()+cam.getRx(), cam.getTy()+cam.getRy(), cam.getTz()+cam.getRz(), 
                            0, 1, 0);
            rot=ang;
            gl.glRotatef(0, 0, 1, 0);
        }
        
        //Dibuja las estrellas
        Estrellas star=new Estrellas(ang);
        //Crea los personajes
        Personaje Ghost = new Personaje(rot*5,400,400);
        Personaje Ghost1 = new Personaje(rot*3f,700,100);
        //Crea la luna con movimiento
        Luna moon =new Luna(ang);
        //Crea la torre
        Torre tower=new Torre();
        //Crea el exterior de la torre
        Exterior ext= new Exterior();
        
        // Flush all drawing operations to the graphics card
        gl.glFlush();
    }

    public void displayChanged(GLAutoDrawable drawable, boolean modeChanged, boolean deviceChanged) {
    }
}

