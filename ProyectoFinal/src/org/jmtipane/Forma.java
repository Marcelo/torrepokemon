/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmtipane;

import javax.media.opengl.GL;
import static org.jmtipane.ProyectoFinal.gl;
import static org.jmtipane.ProyectoFinal.glu;
import static org.jmtipane.ProyectoFinal.glut;
import static org.jmtipane.ProyectoFinal.quad;

/**
 * Proyecto_Final Torre_Pok�mon
 * @author Marcelo Tip�n
 * Fecha: 28/09/2020
 * Clase que crea formas a partir de figuras b�sicas
 */
public class Forma {
    //Variable para dibujar figuras
    Figura fig = new Figura();
    /**
     * Constructor por defecto
     */
    public Forma() {
    }
    
    /**
     * Dibuja el piso final
     */
    public void FinTorre(){
        gl.glPushMatrix();
        //Dibuja la esfera del �ltimo piso
        gl.glColor3f(0.7f, 0.7f, 0.7f);
        gl.glTranslatef(0, 757, 0);
        gl.glScalef(1.2f, 0.7f, 1.2f);
        glut.glutSolidSphere(118, 80, 80);
        //Dibuja l�neas alrededor del cilindro
        gl.glColor3f(0.1f, 0.1f, 0.1f);
        for(int i=0; i<=360 ;i+=15){
                gl.glRotatef(i, 0, 1, 0);
                gl.glBegin(GL.GL_LINES);
                gl.glVertex3f(121,  0, 0);
                gl.glVertex3f(121, -115, 0);
                gl.glEnd();
            }
        //Dibuja un cilindro final
        gl.glColor3f(0.141f, 0.168f, 0.141f);
        gl.glRotatef(90, 1, 0, 0);
        glut.glutSolidCylinder(120, 112, 50, 50);
        gl.glColor3f(0.0f, 0.0f, 0.0f);
        glu.gluDisk(quad, 120, 135, 50, 50);
        //Detalle circular al final de la torre
        gl.glColor3f(0, 0, 0);
        gl.glTranslatef(0, 0, -120);
        glut.glutSolidSphere(15, 25, 25);
        gl.glLineWidth(2);
        gl.glBegin(GL.GL_LINES);
                gl.glVertex3f(0, 0, 0);
                gl.glVertex3f(0, 0, -120);
        gl.glEnd();
        gl.glPopMatrix();
    }
        
    /**
     * Dibuja las gradas
     */
    public void gradas(){
        gl.glPushMatrix();
        gl.glTranslatef(0, 0, 45);
        //borde derecho
        gl.glColor3f(0.098f, 0.098f, 0.098f);
        gl.glBegin(GL.GL_POLYGON);
        gl.glVertex3f( 50,   0, 175);
        gl.glVertex3f( 50, 150, 150);
        gl.glVertex3f( 50, 150, 100);
        gl.glVertex3f( 50,   0, -20);
        gl.glEnd();
        //borde izquierdo
        gl.glBegin(GL.GL_POLYGON);
        gl.glVertex3f( -50,   0, 175);
        gl.glVertex3f( -50, 150, 150);
        gl.glVertex3f( -50, 150, 100);
        gl.glVertex3f( -50,   0, -20);
        gl.glEnd();
        //Crea las gradas a partir de la rotaci�n de rect�ngulos
            for (int i = 0; i < 9; i++) {
                gl.glTranslatef(0, 15, 15);
                fig.rectangulo();
            }
        gl.glPopMatrix();
    }
    
    /**
     * Dibuja la salida de las gradas
     */
    public void borde(){
        gl.glPushMatrix();
        gl.glTranslatef(0, 0, -45);
        //borde bajo derecho
        gl.glColor3f(0.098f, 0.098f, 0.098f);
        gl.glBegin(GL.GL_POLYGON);
        gl.glVertex3f( 50,   0, -325);
        gl.glVertex3f( 50,  50, -315);
        gl.glVertex3f( 50,  50, -200);
        gl.glVertex3f( 50,   0, -200);
        gl.glEnd();
        //borde bajo izquierdo
        gl.glBegin(GL.GL_POLYGON);
        gl.glVertex3f( -50,   0, -325);
        gl.glVertex3f( -50,  50, -315);
        gl.glVertex3f( -50,  50, -200);
        gl.glVertex3f( -50,   0, -200);
        gl.glEnd();
        //base del borde
        gl.glColor3f(0.048f, 0.048f, 0.048f);
        gl.glBegin(GL.GL_POLYGON);
        gl.glVertex3f( -50,   1, -325);
        gl.glVertex3f(  50,   1, -325);
        gl.glVertex3f(  50,   1, -200);
        gl.glVertex3f( -50,   1, -200);
        gl.glEnd();
        gl.glPopMatrix();
    }
    /**
     * Dibuja cada piso
     */
    public void piso(){
        gl.glPushMatrix();
        gl.glScalef(1.2f, 1, 1.2f);
        for(int i=0; i<=270 ;i+=90){
                gl.glRotatef(i, 0, 1, 0);
                fig.Paredes();
            }
            fig.TechoPiso();
            alfombra(0, 0, 0);
        gl.glPopMatrix();
    }
    
    /**
     * Dibuja la pared exterior
     */
    public void paredExt(){
        gl.glPushMatrix();
        gl.glTranslatef(0, -420, 0);
        gl.glScalef(2, 0.5f, 2.2f);
        for(int i=0; i<=270 ;i+=90){
                gl.glRotatef(i, 0, 1, 0);
//                gl.glColor3f(1, 0, 1);
                fig.ParedesExt();
            }
        gl.glPopMatrix();
    }
    
    /**
     * Dibuja las ventanas alrededor del edificio
     */
    public void ventanas(){
        gl.glPushMatrix();
        gl.glScalef(1.205f, 1, 1.205f);
        for(int i=0; i<=360 ;i+=90){
                gl.glRotatef(i, 0, 1, 0);
                fig.ventanas();
            }
        gl.glPopMatrix();
    }
    
    /**
     * Dibuja las ventanas alrededor del primer piso
     */
    public void ventanas1(){
        gl.glPushMatrix();
        gl.glScalef(1.205f, 1, 1.205f);
        for(int i=180; i<=540 ;i+=90){
                gl.glRotatef(i, 0, 1, 0);
                fig.ventanas();
            }
        gl.glPopMatrix();
    }
    
    /**
     * Dibuja la puerta de la torre por fuera
     */
    public void Puerta(){
        gl.glPushMatrix();
            gl.glTranslatef(50, 0, -75);
            gl.glRotatef(90, 0, 1, 0);
        //marco de la puerta
            gl.glBegin(GL.GL_POLYGON);
//            gl.glColor3f(0.25f, 0.18f, 0.129f);
            gl.glColor3f(0.45f, 0.45f, 0.45f);
            gl.glVertex3f(-100,   0.5f, 250.5f);
            gl.glVertex3f( 100,   0.5f, 250.5f);
            gl.glVertex3f( 100, 101.5f, 233.5f);
            gl.glVertex3f(-100, 101.5f, 233.5f);
            gl.glEnd();
        //cuadros de la puerta
            gl.glBegin(GL.GL_POLYGON);
            gl.glColor3f(0.223f, 0.223f, 0.223f);
            gl.glVertex3f(-80,  10.5f, 249.0f);
            gl.glVertex3f(-10,  10.5f, 249.0f);
            gl.glVertex3f(-10,  30.5f, 245.8f);
            gl.glVertex3f(-80,  30.5f, 245.8f);
            gl.glEnd();
            
            gl.glBegin(GL.GL_POLYGON);
            gl.glColor3f(0.223f, 0.223f, 0.223f);
            gl.glVertex3f(-80,  40.5f, 244.5f);
            gl.glVertex3f(-10,  40.5f, 244.5f);
            gl.glVertex3f(-10,  90.5f, 236.5f);
            gl.glVertex3f(-80,  90.5f, 236.5f);
            gl.glEnd();
            
            gl.glBegin(GL.GL_POLYGON);
            gl.glColor3f(0.223f, 0.223f, 0.223f);
            gl.glVertex3f( 10,  10.5f, 249.5f);
            gl.glVertex3f( 80,  10.5f, 249.5f);
            gl.glVertex3f( 80,  30.5f, 246.8f);
            gl.glVertex3f( 10,  30.5f, 246.8f);
            gl.glEnd();
            
            gl.glBegin(GL.GL_POLYGON);
            gl.glColor3f(0.223f, 0.223f, 0.223f);
            gl.glVertex3f( 10,  40.5f, 244.5f);
            gl.glVertex3f( 80,  40.5f, 244.5f);
            gl.glVertex3f( 80,  90.5f, 236.5f);
            gl.glVertex3f( 10,  90.5f, 236.5f);
            gl.glEnd();
            
        //Perillas      
            gl.glBegin(GL.GL_POLYGON);
            gl.glColor3f(0.0f, 0.0f, 0.0f);
            gl.glVertex3f( -8,  41.5f, 244.5f);
            gl.glVertex3f( -3,  41.5f, 244.5f);
            gl.glVertex3f( -3,  46.5f, 243.5f);
            gl.glVertex3f( -8,  46.5f, 243.5f);
            gl.glEnd();
            
            gl.glBegin(GL.GL_POLYGON);
            gl.glColor3f(0.0f, 0.0f, 0.0f);
            gl.glVertex3f( 8,  41.5f, 244.5f);
            gl.glVertex3f( 3,  41.5f, 244.5f);
            gl.glVertex3f( 3,  46.5f, 243.5f);
            gl.glVertex3f( 8,  46.5f, 243.5f);
            gl.glEnd();
            
        //lines de marco de puerta
            gl.glBegin(GL.GL_LINES);
            gl.glLineWidth(1);
            gl.glColor3f(0.10f, 0.10f, 0.10f);
            gl.glVertex3f(90,  0.8f, 251.5f);
            gl.glVertex3f(90, 95.8f, 236.5f);
            
            gl.glVertex3f(0,  0.8f, 251.5f);
            gl.glVertex3f(0, 95.8f, 236.5f);
            
            gl.glVertex3f(-90,  0.8f, 251.5f);
            gl.glVertex3f(-90, 95.8f, 236.5f);
            
            gl.glVertex3f(-90, 95.8f, 236.5f);
            gl.glVertex3f( 90, 95.8f, 236.5f);
            gl.glEnd();
        gl.glPopMatrix();
    }
    
    /**
     * Dibuja la puerta de la torre por el interior
     */
    public void PuertaInt(){
        gl.glPushMatrix();
            
            gl.glTranslatef(45, 0, -75);
            gl.glRotatef(90, 0, 1, 0);
        //marco de la puerta
            gl.glBegin(GL.GL_POLYGON);
//            gl.glColor3f(0.25f, 0.18f, 0.129f);
            gl.glColor3f(0.45f, 0.45f, 0.45f);
            gl.glVertex3f(-100,   0.5f, 250.5f);
            gl.glVertex3f( 100,   0.5f, 250.5f);
            gl.glVertex3f( 100, 101.5f, 233.5f);
            gl.glVertex3f(-100, 101.5f, 233.5f);
            gl.glEnd();
        //cuadros de la puerta
            gl.glBegin(GL.GL_POLYGON);
            gl.glColor3f(0.223f, 0.223f, 0.223f);
            gl.glVertex3f(-80,  10.5f, 246.0f);
            gl.glVertex3f(-10,  10.5f, 246.0f);
            gl.glVertex3f(-10,  30.5f, 242.8f);
            gl.glVertex3f(-80,  30.5f, 242.8f);
            gl.glEnd();
            
            gl.glBegin(GL.GL_POLYGON);
            gl.glColor3f(0.223f, 0.223f, 0.223f);
            gl.glVertex3f(-80,  40.5f, 241.5f);
            gl.glVertex3f(-10,  40.5f, 241.5f);
            gl.glVertex3f(-10,  90.5f, 233.5f);
            gl.glVertex3f(-80,  90.5f, 233.5f);
            gl.glEnd();
            
            gl.glBegin(GL.GL_POLYGON);
            gl.glColor3f(0.223f, 0.223f, 0.223f);
            gl.glVertex3f( 10,  10.5f, 246.5f);
            gl.glVertex3f( 80,  10.5f, 246.5f);
            gl.glVertex3f( 80,  30.5f, 243.8f);
            gl.glVertex3f( 10,  30.5f, 243.8f);
            gl.glEnd();
            
            gl.glBegin(GL.GL_POLYGON);
            gl.glColor3f(0.223f, 0.223f, 0.223f);
            gl.glVertex3f( 10,  40.5f, 241.5f);
            gl.glVertex3f( 80,  40.5f, 241.5f);
            gl.glVertex3f( 80,  90.5f, 233.5f);
            gl.glVertex3f( 10,  90.5f, 233.5f);
            gl.glEnd();
            
        //Perillas      
            gl.glBegin(GL.GL_POLYGON);
            gl.glColor3f(0.0f, 0.0f, 0.0f);
            gl.glVertex3f( -8,  41.5f, 241.5f);
            gl.glVertex3f( -3,  41.5f, 241.5f);
            gl.glVertex3f( -3,  46.5f, 240.5f);
            gl.glVertex3f( -8,  46.5f, 240.5f);
            gl.glEnd();
            
            gl.glBegin(GL.GL_POLYGON);
            gl.glColor3f(0.0f, 0.0f, 0.0f);
            gl.glVertex3f( 8,  41.5f, 241.5f);
            gl.glVertex3f( 3,  41.5f, 241.5f);
            gl.glVertex3f( 3,  46.5f, 240.5f);
            gl.glVertex3f( 8,  46.5f, 240.5f);
            gl.glEnd();
            
        //lines de marco de puerta
            gl.glBegin(GL.GL_LINES);
            gl.glLineWidth(1);
            gl.glColor3f(0.25f, 0.14f, 0.03f);
            gl.glVertex3f(90,  0.8f, 248.5f);
            gl.glVertex3f(90, 95.8f, 233.5f);
            
            gl.glVertex3f(0,  0.8f, 248.5f);
            gl.glVertex3f(0, 95.8f, 233.5f);
            
            gl.glVertex3f(-90,  0.8f, 248.5f);
            gl.glVertex3f(-90, 95.8f, 233.5f);
            
            gl.glVertex3f(-90, 95.8f, 233.5f);
            gl.glVertex3f( 90, 95.8f, 233.5f);
            gl.glEnd();
        gl.glPopMatrix();
    }
    
    /**
     * Dibuja la puerta exterior de la torre por fuera
     */
    public void PuertaExt(){
        gl.glPushMatrix();
            
            gl.glRotatef(18.0f, 0, 0, 1);
            gl.glScalef(1, 0.7f, 1);
            gl.glTranslatef(97, -790, 0);
            gl.glRotatef(90, 0, 1, 0);
            
        //marco de la puerta
            gl.glBegin(GL.GL_POLYGON);
            gl.glColor3f(0.20f, 0.20f, 0.20f);
            gl.glVertex3f(-100,   0.5f, 250.5f);
            gl.glVertex3f( 100,   0.5f, 250.5f);
            gl.glVertex3f( 100, 101.5f, 233.5f);
            gl.glVertex3f(-100, 101.5f, 233.5f);
            gl.glEnd();
            
        //cuadros de la puerta
            gl.glBegin(GL.GL_POLYGON);
            gl.glColor3f(0.30f, 0.30f, 0.30f);
            gl.glVertex3f(-80,  10.5f, 249.5f);
            gl.glVertex3f(-10,  10.5f, 249.5f);
            gl.glVertex3f(-10,  90.5f, 236.5f);
            gl.glVertex3f(-80,  90.5f, 236.5f);
            gl.glEnd();
            
            gl.glBegin(GL.GL_POLYGON);
            gl.glColor3f(0.30f, 0.30f, 0.30f);
            gl.glVertex3f( 10,  10.5f, 249.5f);
            gl.glVertex3f( 80,  10.5f, 249.5f);
            gl.glVertex3f( 80,  90.5f, 236.5f);
            gl.glVertex3f( 10,  90.5f, 236.5f);
            gl.glEnd();
            
        //Perillas      
            gl.glBegin(GL.GL_POLYGON);
            gl.glColor3f(0.0f, 0.0f, 0.0f);
            gl.glVertex3f( -8,  41.5f, 244.5f);
            gl.glVertex3f( -3,  41.5f, 244.5f);
            gl.glVertex3f( -3,  46.5f, 243.5f);
            gl.glVertex3f( -8,  46.5f, 243.5f);
            gl.glEnd();
            
            gl.glBegin(GL.GL_POLYGON);
            gl.glColor3f(0.0f, 0.0f, 0.0f);
            gl.glVertex3f( 8,  41.5f, 244.5f);
            gl.glVertex3f( 3,  41.5f, 244.5f);
            gl.glVertex3f( 3,  46.5f, 243.5f);
            gl.glVertex3f( 8,  46.5f, 243.5f);
            gl.glEnd();
            
        //lines de marco de puerta
            gl.glBegin(GL.GL_LINES);
            gl.glLineWidth(1);
            gl.glColor3f(0.10f, 0.10f, 0.10f);
            gl.glVertex3f(90,  0.8f, 251.5f);
            gl.glVertex3f(90, 95.8f, 236.5f);
            
            gl.glVertex3f(0,  0.8f, 251.5f);
            gl.glVertex3f(0, 95.8f, 236.5f);
            
            gl.glVertex3f(-90,  0.8f, 251.5f);
            gl.glVertex3f(-90, 95.8f, 236.5f);
            
            gl.glVertex3f(-90, 95.8f, 236.5f);
            gl.glVertex3f( 90, 95.8f, 236.5f);
            gl.glEnd();
        gl.glPopMatrix();
    }
    
    /**
     * Dibuja la puerta exterior de la torre por el interior
     */
    public void PuertaExtInt(){
        gl.glPushMatrix();
            
            gl.glRotatef(18.0f, 0, 0, 1);
            gl.glScalef(1, 0.7f, 1);
            gl.glTranslatef(92, -790, 0);
            gl.glRotatef(90, 0, 1, 0);
        //marco de la puerta
            gl.glBegin(GL.GL_POLYGON);
            gl.glColor3f(0.20f, 0.20f, 0.20f);
            gl.glVertex3f(-100,   0.5f, 250.5f);
            gl.glVertex3f( 100,   0.5f, 250.5f);
            gl.glVertex3f( 100, 101.5f, 233.5f);
            gl.glVertex3f(-100, 101.5f, 233.5f);
            gl.glEnd();
        //cuadros de la puerta
            gl.glBegin(GL.GL_POLYGON);
            gl.glColor3f(0.30f, 0.30f, 0.30f);
            gl.glVertex3f(-80,  10.5f, 249.5f);
            gl.glVertex3f(-10,  10.5f, 249.5f);
            gl.glVertex3f(-10,  90.5f, 236.5f);
            gl.glVertex3f(-80,  90.5f, 236.5f);
            gl.glEnd();
            
            gl.glBegin(GL.GL_POLYGON);
            gl.glColor3f(0.30f, 0.30f, 0.30f);
            gl.glVertex3f( 10,  10.5f, 249.5f);
            gl.glVertex3f( 80,  10.5f, 249.5f);
            gl.glVertex3f( 80,  90.5f, 236.5f);
            gl.glVertex3f( 10,  90.5f, 236.5f);
            gl.glEnd();
            
        //Perillas      
            gl.glBegin(GL.GL_POLYGON);
            gl.glColor3f(0.0f, 0.0f, 0.0f);
            gl.glVertex3f( -8,  41.5f, 241.5f);
            gl.glVertex3f( -3,  41.5f, 241.5f);
            gl.glVertex3f( -3,  46.5f, 240.5f);
            gl.glVertex3f( -8,  46.5f, 240.5f);
            gl.glEnd();
            
            gl.glBegin(GL.GL_POLYGON);
            gl.glColor3f(0.0f, 0.0f, 0.0f);
            gl.glVertex3f( 8,  41.5f, 241.5f);
            gl.glVertex3f( 3,  41.5f, 241.5f);
            gl.glVertex3f( 3,  46.5f, 240.5f);
            gl.glVertex3f( 8,  46.5f, 240.5f);
            gl.glEnd();
            
        //lines de marco de puerta
            gl.glBegin(GL.GL_LINES);
            gl.glLineWidth(1);
            gl.glColor3f(0.10f, 0.10f, 0.10f);
            gl.glVertex3f(90,  0.8f, 248.5f);
            gl.glVertex3f(90, 95.8f, 233.5f);
            
            gl.glVertex3f(0,  0.8f, 248.5f);
            gl.glVertex3f(0, 95.8f, 233.5f);
            
            gl.glVertex3f(-90,  0.8f, 248.5f);
            gl.glVertex3f(-90, 95.8f, 233.5f);
            
            gl.glVertex3f(-90, 95.8f, 233.5f);
            gl.glVertex3f( 90, 95.8f, 233.5f);
            gl.glEnd();
//            fig.Circulo(-112.5f, 100, 233.33f, 12.5f);
        gl.glPopMatrix();
    }
    /**
     * Dibuja la base de la torre
     */
    public void baseTorre(){
        gl.glPushMatrix();
        //Dibuja el piso de la torre
        gl.glColor3f(0.198f, 0.198f, 0.198f);
        gl.glBegin(GL.GL_POLYGON);
        gl.glVertex3f( 350, 0,  350);
        gl.glVertex3f(-350, 0,  350);
        gl.glVertex3f(-350, 0, -350);
        gl.glVertex3f( 350, 0, -350);
        gl.glEnd();
        //Dibuja los laterales de la torre
        for(int i=0; i<=360 ;i+=90){
            gl.glRotatef(i, 0, 1, 0);
            fig.BaseTorre();
        }
        gl.glPopMatrix();
    }
    
    /**
     * Dibuja el suelo
     */
    public void suelo(){
        gl.glPushMatrix();
//        gl.glColor3f(0.0f, 0.2f, 0.09f);    //verde
//        gl.glColor3f(0.305f, 0.231f, 0.192f);   //verde oscuro
//        gl.glColor3f(0.176f, 0.066f, 0.023f);   //caf�
//        gl.glColor3f(0.721f, 0.619f, 0.737f);   //rosa claro
        gl.glColor3f(0.603f, 0.458f, 0.627f);   //rosa
//        gl.glColor3f(1.0f, 0.592f, 0.423f);   //rojo
        gl.glBegin(GL.GL_POLYGON);
        gl.glVertex3f( 1200, -20,  1200);
        gl.glVertex3f(-1200, -20,  1200);
        gl.glVertex3f(-1200, -20, -1200);
        gl.glVertex3f( 1200, -20, -1200);
        gl.glEnd();
        gl.glPopMatrix();
    }
    
    /**
     * Dibuja los caminos
     */
    public void caminos(){
        gl.glPushMatrix();
        for(int i=0; i<=360 ;i+=90){
            gl.glRotatef(i, 0, 1, 0);
            fig.CaminoExt();
        }
        gl.glRotatef(90, 0, 1, 0);
        fig.CaminoExt2();
        gl.glPopMatrix();
    }
    
    /**
     * Dibuja una l�pida en una posici�n dada
     * @param x
     * @param y
     * @param z
     * @param ang
     */
    public void lapidaL(float x, float y, float z, float ang){
        gl.glPushMatrix();
        gl.glTranslatef(x, y, z);
        gl.glRotatef(-270+ang, 0, 1, 0);
        gl.glTranslatef(0, -270, 0);
        gl.glLineWidth(2);
        //lineas 
            gl.glColor3f(0.1f, 0.1f, 0.1f);           
            gl.glBegin(GL.GL_LINES);
            gl.glVertex3f( -5, 25,   5.5f);
            gl.glVertex3f(  5, 25,   5.5f);
            gl.glVertex3f( -8, 30,   5.5f);
            gl.glVertex3f(  8, 30,   5.5f);
            gl.glVertex3f(-11, 35,   5.5f);
            gl.glVertex3f( 11, 35,   5.5f);
            gl.glEnd();
        
        //pared izquierda base
        gl.glColor3f(1, 1, 1);
        for(int i=0; i<=360 ;i+=180){
            gl.glRotatef(i, 0, 1, 0);
            gl.glBegin(GL.GL_POLYGON);
            gl.glVertex3f(-20,  0, -15);
            gl.glVertex3f(-20, 10, -15);
            gl.glVertex3f(-20, 10,  15);
            gl.glVertex3f(-20,  0,  15);
            gl.glEnd();
        }
        
        //pared frontal base
        gl.glColor3f(0.6f, 0.6f, 0.6f);
        for(int i=0; i<=360 ;i+=180){
            gl.glRotatef(i, 0, 1, 0);
            gl.glBegin(GL.GL_POLYGON);
            gl.glVertex3f(-20,   0,   15);
            gl.glVertex3f(-20,  10,   15);
            gl.glVertex3f( 20,  10,   15);
            gl.glVertex3f( 20,   0,   15);
            gl.glEnd();
        }
        
        //pared izquierda l�pida
        gl.glColor3f(1, 1, 1);
        for(int i=0; i<=360 ;i+=180){
            gl.glRotatef(i, 0, 1, 0);
            gl.glBegin(GL.GL_POLYGON);
            gl.glVertex3f(-12, 10, -5);
            gl.glVertex3f(-12, 50, -5);
            gl.glVertex3f(-12, 50,  5);
            gl.glVertex3f(-12, 10,  5);
            gl.glEnd();
        }
        
        //pared frontal l�pida
        gl.glColor3f(0.6f, 0.6f, 0.6f);
        for(int i=0; i<=360 ;i+=180){
            gl.glRotatef(i, 0, 1, 0);
            gl.glBegin(GL.GL_POLYGON);
            gl.glVertex3f(-12,  10,   5);
            gl.glVertex3f(-12,  50,   5);
            gl.glVertex3f( 12,  50,   5);
            gl.glVertex3f( 12,  10,   5);
            gl.glEnd();
        }
        
        //techo base
        gl.glColor3f(0, 0, 0);
        gl.glBegin(GL.GL_POLYGON);
        gl.glVertex3f(-20,  10,  -15);
        gl.glVertex3f(-20,  10,   15);
        gl.glVertex3f( 20,  10,   15);
        gl.glVertex3f( 20,  10,  -15);
        gl.glEnd();
        
        //techo l�pida
        gl.glColor3f(0, 0, 0);
        gl.glBegin(GL.GL_POLYGON);
        gl.glVertex3f(-12,  50,  -5);
        gl.glVertex3f(-12,  50,   5);
        gl.glVertex3f( 12,  50,   5);
        gl.glVertex3f( 12,  50,  -5);
        gl.glEnd();
        
        //detalle lapida
        fig.Circulo(0, 42, 5.5f, 3);
        gl.glPopMatrix();
    }
    /**
     * Dibuja un pilar con un cono s�lido
     * @param x
     * @param y
     * @param z
     */
    public void pilar(int x, int y, int z){
        gl.glPushMatrix();
        gl.glTranslatef(x, y, z);
        gl.glTranslatef(0, -400, 0);
        //pared base
        gl.glColor3f(0.6f, 0.6f, 0.6f);
        for(int i=0; i<=360 ;i+=90){
            gl.glRotatef(i, 0, 1, 0);
            gl.glBegin(GL.GL_POLYGON);
            gl.glVertex3f(-20,  0, -20);
            gl.glVertex3f(-20,  5, -20);
            gl.glVertex3f(-20,  5,  20);
            gl.glVertex3f(-20,  0,  20);
            gl.glEnd();
        }
        //techo base
        gl.glColor3f(0.223f, 0.223f, 0.223f);
        gl.glBegin(GL.GL_POLYGON);
        gl.glVertex3f(-20,  5,  -20);
        gl.glVertex3f(-20,  5,   20);
        gl.glVertex3f( 20,  5,   20);
        gl.glVertex3f( 20,  5,  -20);
        gl.glEnd();
        
        //Dibuja el pilar
        gl.glColor3f(0.7f, 0.7f, 0.7f);
        gl.glTranslatef(0, 10, 0);
        gl.glRotatef(-90, 1, 0, 0);
        glut.glutSolidCone(15, 300, 25, 25);
        gl.glPopMatrix();
    }
    /**
     * Dibuja una columna en el interior del edificio
     * @param x
     * @param y
     * @param z
     */
    public void pilarInt(int x, int y, int z){
        gl.glPushMatrix();
        gl.glTranslatef(x, y, z);
        gl.glTranslatef(0, -400, 0);
        //pared base
        gl.glColor3f(0.6f, 0.6f, 0.6f);
        for(int i=0; i<=360 ;i+=90){
            gl.glRotatef(i, 0, 1, 0);
            gl.glBegin(GL.GL_POLYGON);
            gl.glVertex3f(-20,  0, -20);
            gl.glVertex3f(-20,  5, -20);
            gl.glVertex3f(-20,  5,  20);
            gl.glVertex3f(-20,  0,  20);
            gl.glEnd();
        }
        //techo base
        gl.glColor3f(0, 0, 0);
        gl.glBegin(GL.GL_POLYGON);
        gl.glVertex3f(-20,  5,  -20);
        gl.glVertex3f(-20,  5,   20);
        gl.glVertex3f( 20,  5,   20);
        gl.glVertex3f( 20,  5,  -20);
        gl.glEnd();
        
        //Dibuja la columna
        gl.glColor3f(0.7f, 0.7f, 0.7f);
        gl.glTranslatef(0, 1, 0);
        gl.glRotatef(-90, 1, 0, 0);
        glut.glutSolidCylinder(15, 145, 30, 30);
        
        gl.glRotatef(90, 1, 0, 0);
        //Dibuja base superior
        gl.glTranslatef(0, 140, 0);
        for(int i=0; i<=360 ;i+=90){
            gl.glRotatef(i, 0, 1, 0);
            gl.glBegin(GL.GL_POLYGON);
            gl.glVertex3f(-20,  0, -20);
            gl.glVertex3f(-20,  5, -20);
            gl.glVertex3f(-20,  5,  20);
            gl.glVertex3f(-20,  0,  20);
            gl.glEnd();
        }
        //techo base
        gl.glColor3f(0, 0, 0);
        gl.glBegin(GL.GL_POLYGON);
        gl.glVertex3f(-20,  0,  -20);
        gl.glVertex3f(-20,  0,   20);
        gl.glVertex3f( 20,  0,   20);
        gl.glVertex3f( 20,  0,  -20);
        gl.glEnd();
        gl.glPopMatrix();
    }
    /**
     * Dibuja un �rbol
     */
    public void arbol(){
        
        gl.glPushMatrix();
        gl.glTranslatef(-1200, -420, -1200);
        gl.glRotatef(-90, 1, 0, 0);
        
        gl.glColor3f(0.215f, 0.184f, 0.223f);
//        gl.glColor3f(0.4f, 0.36f, 0.21f);
//        gl.glColor3f(0.8f, 0.4f, 0.0f);
        glut.glutSolidCylinder(20, 100, 25, 25);
        
        gl.glTranslatef(0, 0, 40);
//        gl.glColor3f(0.0f, 0.25f, 0.09f); //verde claro
//        gl.glColor3f(0.192f, 0.215f, 0.168f); //verde oscuro
//        gl.glColor3f(0.580f, 0.156f, 0.0f); //rojo
//        gl.glColor3f(0.490f, 0.129f, 0.505f); //purpura
        gl.glColor3f(0.341f, 0.137f, 0.392f); //morado
        glut.glutSolidCone(110, 70, 25, 25);
        
        gl.glTranslatef(0, 0, 35);
//        gl.glColor3f(0.0f, 0.30f, 0.09f); //verde claro
//        gl.glColor3f(0.239f, 0.270f, 0.211f); //verde oscuro
//        gl.glColor3f(0.725f, 0.196f, 0.0f); //rojo
        gl.glColor3f(0.427f, 0.172f, 0.490f); //morado
        glut.glutSolidCone(90, 60, 25, 25);
        
        gl.glTranslatef(0, 0, 35);
//        gl.glColor3f(0.0f, 0.35f, 0.09f); //verde claro
//        gl.glColor3f(0.305f, 0.337f, 0.266f); //verde oscuro
//        gl.glColor3f(0.905f, 0.247f, 0.0f); //rojo
        gl.glColor3f(0.533f, 0.215f, 0.611f); //morado
        glut.glutSolidCone(60, 50, 25, 25);
        
        gl.glTranslatef(0, 0, 35);
//        gl.glColor3f(0.0f, 0.40f, 0.09f); //verde claro
//        gl.glColor3f(0.372f, 0.423f, 0.333f); //verde oscuro
//        gl.glColor3f(1.0f, 0.369f, 0.133f); //rojo
        gl.glColor3f(0.654f, 0.286f, 0.749f); //morado
        glut.glutSolidCone(35, 35, 25, 25);
        
        gl.glPopMatrix();
    }
    
    /**
     * Dibuja una alfombra para cada piso
     * @param x
     * @param y
     * @param z 
     */
    public void alfombra(int x, int y, int z){
        gl.glPushMatrix();
        gl.glTranslatef(x, y, z);
        //alfombra
        gl.glColor3f(0.3f, 0.1f, 0.1f);
            gl.glBegin(GL.GL_POLYGON);
            gl.glVertex3f(-110, 2, -80);
            gl.glVertex3f(-110, 2,  80);
            gl.glVertex3f( 110, 2,  80);
            gl.glVertex3f( 110, 2, -80);
            gl.glEnd();
        gl.glPopMatrix();
    }
}
