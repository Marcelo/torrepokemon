/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmtipane;

import static org.jmtipane.ProyectoFinal.gl;
import static org.jmtipane.ProyectoFinal.glu;
import static org.jmtipane.ProyectoFinal.quad;

/**
 * Proyecto_Final Torre_Pok�mon
 * @author Marcelo Tip�n
 * Fecha: 28/09/2020
 * Clase que dibuja la luna con movimiento individual
 */
public class Luna {
    //Variable para detalles
    Figura fig = new Figura();
    /**
     * Constructor para dibujar la luna a partir de una variable
     * que indica su movimiento
     * @param ang 
     */
    public Luna(float ang){
        gl.glPushMatrix();
        gl.glRotatef(ang, 0.0f, 1.0f, 0.0f);
        gl.glTranslatef(0, 600, 1100);
        gl.glRotatef(ang, 1.0f, 0.0f, 0.0f);
        gl.glColor3f(0.986f, 0.947f, 0.94f);
        glu.gluSphere(quad, 100, 30, 30);
        gl.glPopMatrix();
    }
}
